#include <iostream>
#include <ANN.h>
#include <vector>
using namespace std;
using namespace ANN;

int main()
{
	std::vector<std::vector<float>> vhod;
	std::vector<std::vector<float>> vyhod; 
	string adress = "R:\\�������\\Int Sist\\machine_learning_fzf\\example.txt";

	LoadData(adress,vhod, vyhod);
	vector<int> config;
	config.resize(4);
	config[0] = 2;
	config[1] = 10;
	config[2] = 10;
	config[3] = 1;
	
	auto ann = CreateNeuralNetwork(config, ANeuralNetwork::POSITIVE_SYGMOID);
	std::cout << ann->GetType().c_str() << endl;
	//BackPropTrain(ann, vhod, vyhod, 100000, 1.e-2, 0.1, true);
	
	ann->Save("ann.ann");

	getchar();
}